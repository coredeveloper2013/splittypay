import Home from '../components/home.vue'
import CardInfo from '../components/cardInfo.vue'
import Checkpoint from '../components/checkpoint.vue'
import Monitor from '../components/monitor.vue'

export const routes = [
    {path : '/' , component : Home},
    {path : '/card/details' , component : CardInfo},
    {path : '/payment/checkpoint' , component : Checkpoint},
    {path : '/payment/monitor' , component : Monitor}
];