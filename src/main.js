import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import {routes} from './routes/router.js'

// Plugin
// ==================
import axios from 'axios'
import VueAxios from 'vue-axios'
import {store} from './store/store'

import { Dropdown } from 'bootstrap-vue/es/components';
Vue.use(Dropdown);


window.$ = window.jQuery = require('jquery');
Vue.use(VueAxios, axios);
Vue.use(VueRouter);


import { MdField, MdButton, MdMenu, MdIcon, MdList } from 'vue-material/dist/components'
Vue.use(MdField)
Vue.use(MdButton)
Vue.use(MdMenu)
Vue.use(MdIcon)
Vue.use(MdList)


import Authentication from './mixin/auth';
const Mixins = Object.assign({}, Authentication);
Vue.mixin({
    methods : Mixins
});

const router = new VueRouter({
    routes: routes,
    mode: 'history'
});

global.core = {
    // APP: 'http://localhost:8080',
    // APP_Path: 'http://localhost/projects/mediusware/spay/spay',
    APP: 'https://mediusware.com/demo/splittypay',
    APP_Path: 'https://mediusware.com/demo/splittypay',
};


new Vue({
    el: '#app',
    store: store,
    router: router,
    render: h => h(App)
});
